package kpaymentgateway

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type ErrorResponse struct {
	Object  string `json:"object"`
	Code    string     `json:"code"`
	Message string     `json:"message"`
}

func (e ErrorResponse) Error() string {
	return fmt.Sprintf("%s: %s", e.Code, e.Message)
}


type CreateChargeRequest struct {
	Amount         int64    `json:"amount"`
	Currency       string         `json:"currency"`
	Description    string         `json:"description"`
	SourceType     string     `json:"source_type"`
	Mode           string `json:"mode,omitempty"`
	ReferenceOrder string         `json:"reference_order"`
	Token          string         `json:"token,omitempty"`
	SaveCard       bool           `json:"savecard,omitempty"`
	OrderID        string         `json:"order_id,omitempty"`
	Ref1           string         `json:"ref_1,omitempty"`
	Ref2           string         `json:"ref_2,omitempty"`
	Ref3           string         `json:"ref_3,omitempty"`
	Customer       struct {
		CustomerID string `json:"customer_id,omitempty"`
		CardID     string `json:"card_id,omitempty"`
	} `json:"customer,omitempty"`
	AdditionalData struct {
		MID string `json:"mid"`
		TID string `json:"tid,omitempty"`
	} `json:"additional_data"`
}

type VoidChargeRequest struct {
	Reason string `json:"reason"`
}

type ChargeResponse struct {
	ID               string           `json:"id"`
	Object           string       `json:"object"`
	Amount           int64      `json:"amount"`
	Currency         string           `json:"currency"`
	TransactionState string `json:"transaction_state"`
	Source           struct {
		ID          string `json:"id"`
		Object      string `json:"object"`
		Brand       string `json:"brand"`
		CardMasking string `json:"card_masking"`
		IssuerBank  string `json:"issuer_bank"`
	} `json:"source"`
	Created        Time   `json:"created"`
	Status         string `json:"status"`
	ReferenceOrder string `json:"reference_order"`
	Description    string `json:"description"`
	RedirectURL    string `json:"redirect_url"`
	ApprovalCode   string `json:"approval_code"`
	Ref1           string `json:"ref_1,omitempty"`
	Ref2           string `json:"ref_2,omitempty"`
	Ref3           string `json:"ref_3,omitempty"`
	LiveMode       bool   `json:"livemode"`
	FailureCode    string `json:"failure_code,omitempty"`
	FailureMessage string `json:"failure_message,omitempty"`
	Checksum       string `json:"checksum,omitempty"`
}


type DecimalCent int64

func (c DecimalCent) String() string {
	return fmt.Sprintf("%d.%02d", c/100, c%100)
}
func (c DecimalCent) MarshalJSON() ([]byte, error) {
	return []byte(c.String()), nil
}
func (c *DecimalCent) UnmarshalJSON(b []byte) error {
	s := string(b)
	if s == "null" {
		return nil
	}
	ss := strings.SplitN(s, ".", 2)
	baht, err := strconv.ParseInt(ss[0], 10, 64)
	if err != nil {
		return err
	}
	cent := int64(0)
	if len(ss) > 1 {
		if len(ss[1]) == 1 {
			ss[1] += "0"
		}
		cent, err = strconv.ParseInt(ss[1][:2], 10, 64)
		if err != nil {
			return err
		}
	}
	*c = DecimalCent(baht*100 + cent)
	return nil
}


type Time time.Time

func (t Time) String() string {
	s := time.Time(t).Format("20060102150405.000")
	return strings.Replace(s, ".", "", 1)
}
func (t Time) MarshalText() ([]byte, error) {
	return []byte(t.String()), nil
}
func (t *Time) UnmarshalText(b []byte) error {
	s := string(b)
	s = s[:14] + "." + s[14:]
	loc := time.FixedZone("UTC+7", 3600*7)
	tt, err := time.ParseInLocation("20060102150405.000", s, loc)
	if err != nil {
		return err
	}
	*t = Time(tt)
	return nil
}
