package kpaymentgateway



//go:generate mockery --name KPaymentGatewayExternalAPI --inpackage

type KPaymentGatewayExternalAPI interface {
	CreateCharge(apiKey string, chrgReq CreateChargeRequest) (ChargeResponse, error)
	InquireCharge(apiKey string, id string) (ChargeResponse, error)
	VoidCharge(apiKey string, id string, voidReq VoidChargeRequest) (ChargeResponse, error)
}

