package config


var Conf Config

type Config struct {
	MongoDB     struct {
		URI      string `yaml:"uri"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Schema   string `yaml:"schema"`
	} `yaml:"mongodb"`
	KPGW struct{
		MerchantID string
		TerminalID string
		SecretKey string
	}
}

