module go_test_table

go 1.16

require (
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.7.3
)
