package service

import (
	"errors"
	"go_test_table/config"
	"go_test_table/database"
	"go_test_table/kpaymentgateway"
	"go_test_table/model"
)

type PaymentService interface {
	InitPayment(req model.KPGWCardOrdersPaymentRequest) (model.KPGWCardOrdersPaymentResponse, error) //Create Charge
	ConfirmPaymentCallback(chargeID string, token string, status bool, message string, saveCard bool) (model.KPGWCardCallbackRedirectParam, error)
	InquiryPayment(chrg kpaymentgateway.ChargeResponse) error
}

type kbankPGWPaymentServiceImpl struct {
	db   database.KPGWDatabase
	kpgw kpaymentgateway.KPaymentGatewayExternalAPI
	oa   OrderService
}

func (s kbankPGWPaymentServiceImpl) InitPayment(req model.KPGWCardOrdersPaymentRequest) (model.KPGWCardOrdersPaymentResponse, error) {

	if req.OrderID == "" {
		return model.KPGWCardOrdersPaymentResponse{}, errors.New("kpgw init card payment failed: blank input")
	}

	og, err := s.oa.GetOrder(req.OrderID)
	if err != nil {
		return model.KPGWCardOrdersPaymentResponse{}, errors.New("kpgw init card payment failed: cannot get orders")
	}

	chrgReq := NewChargeRequest(og, req)
	chrgResp, err := s.kpgw.CreateCharge(config.Conf.KPGW.SecretKey, chrgReq)
	if err != nil {
		return model.KPGWCardOrdersPaymentResponse{}, errors.New("kpgw init card payment failed: payment gateway failed")
	}

	var resp model.KPGWCardOrdersPaymentResponse
	resp.Redirect = false
	if chrgResp.TransactionState == "Pre-Authorized" {
		// 3D-secure case
		resp.Redirect = true
		resp.RedirectURL = chrgResp.RedirectURL
	}

	return resp, nil
}

func NewChargeRequest(og model.Order, req model.KPGWCardOrdersPaymentRequest) kpaymentgateway.CreateChargeRequest {
	var chrgReq kpaymentgateway.CreateChargeRequest
	chrgReq.Amount = og.Price
	chrgReq.Currency = "THB"
	chrgReq.SourceType = "card"
	chrgReq.Mode = "token"
	chrgReq.Token = req.Token

	chrgReq.ReferenceOrder = og.Reference
	chrgReq.SaveCard = req.SaveCard
	chrgReq.AdditionalData.MID = config.Conf.KPGW.MerchantID
	chrgReq.AdditionalData.TID = config.Conf.KPGW.TerminalID
	return chrgReq
}

func (k kbankPGWPaymentServiceImpl) ConfirmPaymentCallback(chargeID string, token string, status bool, message string, saveCard bool) (model.KPGWCardCallbackRedirectParam, error) {
	panic("implement me")
}

func (k kbankPGWPaymentServiceImpl) InquiryPayment(chrg kpaymentgateway.ChargeResponse) error {
	panic("implement me")
}
