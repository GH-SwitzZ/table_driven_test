package service

import (
	"go_test_table/model"
)

//go:generate mockery --name OrderService --inpackage

type OrderService interface {
	CreateOrder(order model.Order) (model.Order, error)
	GetOrder(id string) (model.Order, error)
	EditOrder(id string, order model.Order) error
}
