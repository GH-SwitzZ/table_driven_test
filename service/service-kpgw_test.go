package service

import (
	"errors"
	"go_test_table/config"
	"go_test_table/database"
	"go_test_table/kpaymentgateway"
	"go_test_table/model"
	"reflect"
	"testing"
)

func Test_kbankPGWPaymentServiceImpl_InitPayment(t *testing.T) {
	type fields struct {
		db   database.KPGWDatabase
		kpgw kpaymentgateway.KPaymentGatewayExternalAPI
		oa   OrderService
	}
	type args struct {
		req model.KPGWCardOrdersPaymentRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.KPGWCardOrdersPaymentResponse
		wantErr bool
	}{
		{
			name: "GetOrder/Error",
			fields: fields{
				oa: func() OrderService {
					m := &MockOrderService{}
					m.On("GetOrder", "oid_1234").
						Return(model.Order{}, errors.New("get order error"))
					return m
				}(),
			},
			args: args{
				req: model.KPGWCardOrdersPaymentRequest{OrderID: "oid_1234"},
			},
			want:    model.KPGWCardOrdersPaymentResponse{},
			wantErr: true,
		},
		{
			name: "Success",
			fields: fields{
				oa: func() OrderService {
					m := &MockOrderService{}
					m.On("GetOrder", "oid_1234").
						Return(model.Order{Reference:"r1234",Price:20000},nil)
					return m
				}(),
				kpgw: func() kpaymentgateway.KPaymentGatewayExternalAPI {
					og:=model.Order{Reference:"r1234",Price:20000}
					req:=model.KPGWCardOrdersPaymentRequest{OrderID: "oid_1234",Token:"t_1234",SaveCard:true}
					chrgReq := NewChargeRequest(og, req)

					m := &kpaymentgateway.MockKPaymentGatewayExternalAPI{}
					m.On("CreateCharge", config.Conf.KPGW.SecretKey,chrgReq).
						Return(kpaymentgateway.ChargeResponse{TransactionState:"Authorized"},nil)
					return m
				}(),
			},
			args: args{
				req: model.KPGWCardOrdersPaymentRequest{OrderID: "oid_1234",Token:"t_1234",SaveCard:true},
			},
			want:    model.KPGWCardOrdersPaymentResponse{Redirect:false},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := kbankPGWPaymentServiceImpl{
				db:   tt.fields.db,
				kpgw: tt.fields.kpgw,
				oa:   tt.fields.oa,
			}
			got, err := s.InitPayment(tt.args.req)
			if (err != nil) != tt.wantErr {
				t.Errorf("InitPayment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitPayment() got = %v, want %v", got, tt.want)
			}
		})
	}
}
