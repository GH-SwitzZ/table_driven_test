package model

import "testing"

func TestLetterGradeWithError(t *testing.T) {
	type args struct {
		score int
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{"Success/GradeA", args{score: 85}, "A", false},
		{"Success/GradeB", args{score: 75}, "B", false},
		{"Success/GradeC", args{score: 25}, "C", false},
		{"Success/GradeD", args{score: 55}, "D", false},
		{"Success/GradeF", args{score: 45}, "F", false},
		{"Failed/Minus", args{score: -1}, "", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := LetterGradeWithError(tt.args.score)
			if (err != nil) != tt.wantErr {
				t.Errorf("LetterGradeWithError() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("LetterGradeWithError() got = %v, want %v", got, tt.want)
			}
		})
	}
}
