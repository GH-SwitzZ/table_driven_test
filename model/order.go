package model

import (
	"time"
)

type Order struct {
	ID        string `bson:"-" json:"order_id"`
	Reference string `bson:"reference" json:"reference"`

	Price  int64  `bson:"price" json:"price"`
	Items  []Item `bson:"items" json:"items"`
	Status string `bson:"status" json:"status"`
	Owner  string `bson:"owner" json:"owner"`

	PaymentInitiated       bool       `bson:"payment_initiated" json:"payment_initiated"`
	PaymentInitiatedAt     *time.Time `bson:"payment_initiated_at,omitempty" json:"payment_initiated_at,omitempty"`
	PaymentInitiatedBy     *string    `bson:"payment_initiated_by,omitempty" json:"payment_initiated_by,omitempty"`
	PaymentInitiatedMethod string     `bson:"payment_initiated_method,omitempty" json:"payment_initiated_method,omitempty"`
	PaymentVoid            bool       `bson:"payment_void,omitempty" json:"payment_void,omitempty"`

	CreatedAt time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt time.Time `bson:"updated_at" json:"updated_at"`
	CreatedBy string    `bson:"created_by" json:"created_by"`
	UpdatedBy string    `bson:"updated_by" json:"updated_by"`
}

type Item struct {
	ItemID   string `bson:"id" json:"id"`
	Quantity int64  `bson:"quantity" json:"quantity"`
	Price    int64  `bson:"price" json:"price"`
}
