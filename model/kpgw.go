package model

import (
	"time"
)

type KPGWCardOrdersPaymentRequest struct {
	OrderID  string `json:"order_id"`
	Token    string `json:"token"`
	MID      string `json:"mid"`
	SaveCard bool   `json:"savecard"`
	CardID   string `json:"cardid"`
}
type KPGWCardOrdersPaymentResponse struct {
	Redirect    bool   `json:"redirect"`
	RedirectURL string `json:"redirect_url,omitempty"`
}

type ChargeEntry struct {
	ChargeID        string    `bson:"charge_id" json:"charge_id"`
	OrdersGroupID   string    `bson:"orders_group_id" json:"orders_group_id"`
	PaymentMethod   string    `bson:"payment_method" json:"payment_method"`
	Reference       string    `bson:"reference" json:"reference"`
	PaipaMerchantID string    `bson:"paipa_merchant_id" json:"paipa_merchant_id"`
	Amount          int64     `bson:"amount" json:"amount"`
	Status          string    `bson:"status" json:"status"`
	CreatedAt       time.Time `bson:"created_at" json:"created_at"`
	UpdatedAt       time.Time `bson:"updated_at" json:"updated_at"`
}

type KPGWCardCallbackRedirectParam struct {
	MerchantID string
	ActivityID  string
}