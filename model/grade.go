package model

import "errors"

func LetterGradeWithError(score int) (string, error) {
	if score < 0 || score > 100 {
		return "", errors.New("score out of range")
	}
	if score >= 80 {
		return "A",nil
	}
	if score >= 70 {
		return "B",nil
	}
	if score >= 60 {
		return "C",nil
	}
	if score >= 50 {
		return "D",nil
	}
	return "F", nil
}


