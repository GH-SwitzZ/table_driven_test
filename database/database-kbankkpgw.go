package database

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go_test_table/model"
	"time"
)


type KPGWDatabase interface {
	CreateChargeEntry(charge model.ChargeEntry) error
	GetChargeEntryByChargeID(chargeID string) (model.ChargeEntry, error)
	UpdateChargeEntryStatus(chargeID string, status string, updatedAt time.Time) error
}


type KPGWDatabaseImpl struct {
	client *mongo.Database
}

func (db *KPGWDatabaseImpl) CreateChargeEntry(charge model.ChargeEntry) error {
	if _, err := db.client.Collection("charge").InsertOne(context.TODO(), charge); err != nil {
		return err
	}
	return nil
}

func (db *KPGWDatabaseImpl) GetChargeEntryByChargeID(chargeID string) (model.ChargeEntry, error) {
	res := db.client.Collection("charge").FindOne(context.TODO(), bson.D{
		{Key: "charge_id", Value: chargeID},
	})
	var charge model.ChargeEntry
	if err := res.Decode(&charge); err != nil {
		return model.ChargeEntry{}, err
	}
	return charge, nil
}

func (db *KPGWDatabaseImpl) UpdateChargeEntryStatus(chargeID string, status string, updatedAt time.Time) error {
	res, err := db.client.Collection("charge").UpdateOne(context.TODO(), bson.D{
		{Key: "charge_id", Value: chargeID},
	}, bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "status", Value: status},
			{Key: "updated_at", Value: updatedAt},
		}},
	})
	if err != nil {
		return err
	}
	if res.MatchedCount == 0 {
		return errors.New("error not found")
	}
	return nil
}



type KPGWDatabaseImplV2 struct {
	client *mongo.Database
}
