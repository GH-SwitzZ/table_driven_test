package database

import (
	"go_test_table/model"
)

type OrderDatabase interface {
	CreateOrder(order model.Order) (model.Order, error)
	GetOrderByID(id string) (model.Order, error)
}
